<?php
include_once("siteinc/common.php");

$response = "<h2 class=\"txtRed\">卜卦失敗，重新求籤</h2>";

$type   = isset($_POST["type"]) ? (int)$_POST["type"] : "";
$phone  = isset($_POST["phone"]) ? $_POST["phone"] : "";

if(!empty($type) && !empty($phone) && ($type>=1 && $type<=17))
{
    $rand       = $guaObj->get1SetGua();
    $guaAns     = array($rand[2], $rand[3]) ;
    $retGuaDesc = $guaObj->getGuaDesc( join( $guaAns), $type);

    $response = "<h2>卦象</h2>";
	$response .= "<p>".$retGuaDesc["div_name"]."(".$retGuaDesc["div_type"].")</p>";
	$response .= "<h2 class=\"txtRed\">".$retGuaDesc["div_attr"]."</h2>";
	$response .= "<hr>";
	$response .= "<h2>象曰</h2>";
	$response .= "<p>".$retGuaDesc["div_view"]."</p>";
	$response .= "<h2>斷卦解義</h2>";
	$response .= "<p>".$retGuaDesc["div_desc"]."</p>";
	$response .= "<h2>解卦釋義</h2>";
	$response .= "<p>".$retGuaDesc["a_desc"]."</p>";

    // 紀錄
    $logFileName    = "result.log" ;
	$fp = fopen($logFileName , "a" ) ;
	$nowTime = date("Y-m-d H:i:s");
	fwrite($fp,"{$nowTime} | {$phone}\r\n") ;
    fclose($fp);
}

echo $response;exit;