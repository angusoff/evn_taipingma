可，多與前輩及同事朋友請益，交流心得，不可因自身專業技能而過度自傲自滿，需重視團隊合作，必能成功。
