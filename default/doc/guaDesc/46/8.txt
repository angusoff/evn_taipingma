對象即將出現，此時需多參與聯誼交流活動，應可遇到好對象。如已有心儀對象，不可直接表白，以免自毀長城。應以循序漸進的方式，婉轉低調的追求，把握時機在出擊，則成功有望。
