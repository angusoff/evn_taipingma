諸事不順，生意恐有一落千丈之虞。此實更應保守以對，不可賭氣投機或逆勢擴張，需知前方陰暗未明，貿然行動，只怕凶多吉少。仔細觀察市場趨勢走向，伺機而動，調整經營方向，以應對即將來到的變局。
