$(document).ready(function() {
    $.shake({
        callback: function() {
            shake();
        }
    });

    $('#btnReload').click(function() {
	    location.reload();
	});

    /*廣告輪播
    $(".ad").hide();
    $(".cycle-slideshow").html('');
    $.ajax({
        url: 'ajax_ad.php',
        dataType:'json',
        success:function(resp){
            if(resp.length>0){
                var html = '';
                for(var i=0;i<resp.length;i++){
                    html += '<a href="'+resp[i].href+'"><img src="'+resp[i].img+'" alt="" /></a>';
                }
                $(".cycle-slideshow").html(html);
                $(".ad").show();
            }
        }
    });*/

});

function play(userClick){
	var audio = document.getElementById("sound");
	if (userClick){
		audio.play();
		audio.pause();
		validate();
	}else{
		audio.play();
	}
}

function validate (){
	//格式檢查
	$phoneChecking=IsPhone($("#phone").val());
	$category = $('#category').val();

	if(!$phoneChecking){
		alert("請填寫正確的手機格式");
	}else if($category == -1){
		alert("請選擇問事類別");
	}else if(($phoneChecking) && ($category != -1)){
        console.log( $('#phone').val());
        console.log( $('#category').val());
        var inPhone = $('#phone').val() ;

        $.ajax({
            url: 'checkPhoneCnt.php',
            type: 'POST',
            dataType: 'html',
            data: {phone: inPhone},
        })
        .done(function(data) {
            console.log("success");
            console.log(data);
            if ( data > 2) {
                alert("己超過三次卜卦上限") ;
            } else {
				$('.pageWrap').fadeOut();
				$('.shakingAlertWrap').css('display','flex').prop('shakable',true);
				$('#phone').blur() ;
				$('#categoory').blur() ;
				shake() ;
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

	}
}

/**
 * [IsPhone 檢查電話號碼]
 * @param   {[type]}   phone [description]
 * @Another Angus
 * @date    2019-05-14
 */
function IsPhone(phone) {
	var regex = /^\(?\d{2}\)?[\s\-]?\d{4}\-?\d{4}$/;

	if(!regex.test(phone)) {
		return false;
	}else{
		return true;
	}
}

function shake (){
    var shakable = $('.shakingAlertWrap').prop('shakable');
    var shakable = true ;
	if(shakable){
        $("#resultMsg").html('<h2 class=\"txtRed\">卜卦失敗，重新求籤</h2>');
        $(".resultIcon").attr("src","default/images/status3.png");
        //求籤
        $.ajax({
            url: 'ajax_result.php',
            type:'post',
            data:{type: $('#category').val(),phone: $("#phone").val()},
            success:function(resp){
                $("#resultMsg").html(resp);
                var target = $("#resultMsg").find(".txtRed").text();
                //alert(target);
                if(typeof target != "undefined"){
                    var statusArr = ["大吉","吉","小吉"];
                    if($.inArray(target,statusArr) != -1){
                        $(".resultIcon").attr("src","default/images/status1.png");
                    }
                    else if(target=="平"){
                        $(".resultIcon").attr("src","default/images/status2.png");
                    }
                }

            }
        });

		$(".shakingAlert").fadeOut("normal",function(){
			$('.result').fadeIn("normal",function(){
				$('.result').fadeOut(400,function(){
					play(false);
					$('.resultOpen').delay(400).fadeIn(400,function(){
						$('.shakingAlertWrap').delay(800).fadeOut(400,function(){
							$('#showArea').addClass("move");
							$('.shakingAlertWrap').prop('shakable',false);
						});
					});
				})
			});
		});
	}
}
