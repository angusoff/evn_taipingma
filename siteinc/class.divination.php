<?php
/**
 * 卜卦類別
 */

class Divination {
	/**
	 * 取得亂數正反
	 */
	private function randOne () {
		return rand( 0, 10000) % 2;
	}

	/**
	 * 取得單一上卦 or 下卦
	 */
	public function getOenGua() {
		$retStr = "" ;
		for ($i=0; $i < 3; $i++) {
			$retStr = $this->randOne().$retStr ;
		}
		return $retStr ;
	}

	/**
	 * 取得一整組卦象
	 */
	public function get1SetGua() {
		$topGan		= $this->getOenGua() ;
		$downGan	= $this->getOenGua() ;

		$tT = $this->hexagrams( $topGan) ;
		$tD = $this->hexagrams( $downGan) ;

		return array( $topGan, $downGan, $tT, $tD, $tT.$tD) ;
	}

	// 取占卜結果靜態檔
	public function getGuaDesc( $thisGua, $thingType="")
	{
		$mainPath = DOC_PATH ;
		$path = $mainPath . "/mainGua/{$thisGua}.txt" ;
		$lines = file( $path) ;
		list($div_sn, $div_sn2, $div_sn3, $div_name, $div_view, $div_type, $div_desc, $div_attr) = explode("|", $lines[0]) ;
		// div_sn, div_sn2, div_sn3, div_name, div_view, div_type, div_desc

		$path = $mainPath . "/guaDesc/{$thisGua}/{$thingType}.txt" ;
		if ( is_file($path)) {
			$lines = file( $path) ;
			$a_desc = $lines[0] ;
		} else {
			$a_desc = "" ;
		}

		return array(
				"div_sn"	=> $div_sn,
				"div_sn2"	=> $div_sn2,
				"div_sn3"	=> $div_sn3,
				"div_name"	=> $div_name,
				"div_view"	=> $div_view,
				"div_type"	=> $div_type,
				"div_desc"	=> $div_desc,
				"div_attr"	=> $div_attr,
				"a_desc"	=> $a_desc,
		) ;
	}

	// 亂數對應卦象
	public function hexagrams ( $getGua) {

		switch ( $getGua) {
			case "111":
				$hexagrams = "0" ;
				break ;
			case "110":
				$hexagrams = "1" ;
				break ;
			case "101":
				$hexagrams = "2" ;
				break ;
			case "100":
				$hexagrams = "3" ;
				break ;
			case "011":
				$hexagrams = "4" ;
				break ;
			case "010":
				$hexagrams = "5" ;
				break ;
			case "001":
				$hexagrams = "6" ;
				break ;
			case "000":
				$hexagrams = "7" ;
				break ;
		}

		return $hexagrams ;
	}

}