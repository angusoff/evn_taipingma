<?php
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
date_default_timezone_set("Asia/Taipei") ;

define("DOC_PATH", "/Applications/XAMPP/xamppfiles/htdocs/TaipingMa/default/doc") ;

include_once ( "class.divination.php");

session_start();
$guaObj = new Divination() ;

function pre($val, $className="", $fileName="", $Parameters = "")
{
	echo '<pre>';
	echo trim($fileName)	? "file Name : {$fileName}\n"		: "" ;
	echo trim($className)	? "class Name : {$className}\n"		: "" ;
	echo trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
	print_r( $val);
	// var_dump( $val);
	echo '</pre>';
}